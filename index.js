'user strict'

const admin = require('firebase-admin');
var AWS = require('aws-sdk');
var async = require('async');

const serviceAccount = require('./serviceAccountKey.json');
AWS.config.update({
    region: 'ap-southeast-1', 
    accessKeyId: "AKIAI4ZSROBERLRX5FPQ", 
    secretAccessKey: "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"
});
var ddb = new AWS.DynamoDB({apiVersion:'2012-08-10'});

const initializeSDK = function () {
    if (admin.apps.length == 0) {
        admin.initializeApp({
            credential: admin.credential.cert(serviceAccount),
            databaseURL: "https://px-dev-436a2.firebaseio.com"
        });
    }
}

const generateIamPolicy = (effect, resource, data) => {
    const authResponse = {};
    const user = {};

    data ? authResponse.principalId = data.user_id : 'unavailable';

    if (data) {
        user.email = data.email;
        user.email_verified = data.email_verified;
        authResponse.context = user;
    }

    if (effect && resource) {
        const policyDocument = {};
        policyDocument.Version = '2012-10-17';
        policyDocument.Statement = [];
        const statementOne = {};
        statementOne.Action = 'execute-api:Invoke';
        statementOne.Effect = effect;
        statementOne.Resource = resource;
        policyDocument.Statement[0] = statementOne;
        authResponse.policyDocument = policyDocument
        
    }

    return authResponse;
}

module.exports.firebase = (event, context) => {
    try {
        initializeSDK();
        var arnList = [];
        var roles = [];
        var fireDB = admin.firestore();
        console.log('sebelum cek token');
        if(!event.authorizationToken) {
            return context.fail('Unauthorized');
        }

        const tokenParts = event.authorizationToken.split(' ');
        const tokenValue = tokenParts[1];

        //Token prefix must be 'bearer' 
        if(!(tokenParts[0].toLowerCase() === 'bearer' && tokenValue)) {
            console.log('should be bearer');
            return context.fail('Unauthorized');;
        }

        console.log('setelah cek token');
        

        var getEmail = function (cb) {
            admin.auth().verifyIdToken(tokenValue).then((userid) => {
                user = userid;
                console.log(JSON.stringify(user));
                email = userid.firebase.identities.email.pop();
                console.log(email);
                return cb(null, email)
                
            }).catch((error) => {
                return cb(error);
            })
        }
        var getUserRoles = function(email, cb) {
            fireDB.collection('user').select('roles').where('email', 'array-contains', email).get()
                .then((snapshot) => {
                    snapshot.forEach((result) => {
                        let role = result.data().roles;
                        roles.push(role);
                    })
                    console.log(roles);
                    return cb(null, roles);
                }
            ).catch((error) => {
                return cb(error);
            })
        }
        var getArnData = function(roles, cb) {
            var params = {
                TableName: "arnListRoleBased",
                Key: {
                    "role": {
                        "S": roles.pop()
                    }                                
                }
            }
            var getArnDataPromise = ddb.getItem(params).promise();
            getArnDataPromise.then((data, err) => {
                if (err) {
                    return cb(err)
                } else {
                    if (data.Item) {
                        let arnData = [];
                        arnData = data.Item.arn.SS
                        arnData.forEach((arn) => {
                            arnList.push(arn);
                        })
                    }
                    console.log(data)
                    return cb(null, arnList);
                }
            })
        }
        async.waterfall(
            [
                getEmail,
                getUserRoles,
                getArnData
            ],
            async function(err, arnList) {
                if(err) {
                    console.log(err.message);
                    return context.fail('Unauthorized');
                }
                else {
                    var policy = generateIamPolicy('Allow', arnList, user);
                    console.log(JSON.stringify(policy));
                    return context.succeed(policy);
                }
            }
        )
    } catch (error) {
        console.log(error);
        return context.fail('Unauthorized');
        //return generateIamPolicy('Deny', null, null);
    }
}
